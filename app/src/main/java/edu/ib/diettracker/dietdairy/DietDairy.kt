package edu.ib.diettracker.dietdairy

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import edu.ib.diettracker.R

class DietDairy : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.calories)

        val buttonClick_do_plusa = findViewById<ImageButton>(R.id.buton_jakistam)
        buttonClick_do_plusa.setOnClickListener {
            val intent = Intent(this, SearchLope::class.java)
            startActivity(intent)
        }
    }
}