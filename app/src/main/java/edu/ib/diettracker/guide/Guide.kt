package edu.ib.diettracker.guide

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import edu.ib.diettracker.R

class Guide : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.guide)

        val inputMass = findViewById<EditText>(R.id.putMass)
        val inputHeigh = findViewById<EditText>(R.id.putHeigh)
        val BMIButtonn = findViewById<Button>(R.id.BMIbutton)

        val wynik = findViewById<TextView>(R.id.drukujWynik)

        BMIButtonn.setOnClickListener {
            var wartosc: Double? = null
            var heightValue = 0.0
            var weightValue = 0.0

            weightValue = inputMass?.text.toString().toDouble()
            heightValue = inputHeigh?.text.toString().toDouble()

            wartosc = (weightValue) / ((heightValue/100) * (heightValue/100))

            val bmiDouble = wartosc.toDouble()

            wynik.text = "Your BMI is ${String.format("%.2f",bmiDouble)}"
            //wynik= wartosc.toString()

        }

    }
}