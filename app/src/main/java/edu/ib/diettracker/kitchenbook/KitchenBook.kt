package edu.ib.diettracker.kitchenbook

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import edu.ib.diettracker.R

class KitchenBook : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.kitchen_book)
    }
}