package edu.ib.diettracker

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import edu.ib.diettracker.dietdairy.DietDairy
import edu.ib.diettracker.goals.Goals
import edu.ib.diettracker.guide.Guide
import edu.ib.diettracker.kitchenbook.KitchenBook
import edu.ib.diettracker.progress.Progress

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buttonClick1 = findViewById<ImageButton>(R.id.button1)
        buttonClick1.setOnClickListener {
            val intent = Intent(this, DietDairy::class.java)
            startActivity(intent)
        }

        val buttonClick2 = findViewById<ImageButton>(R.id.button2)
        buttonClick2.setOnClickListener {
            val intent = Intent(this, Guide::class.java)
            startActivity(intent)
        }

        val buttonClick3 = findViewById<ImageButton>(R.id.button3)
        buttonClick3.setOnClickListener {
            val intent = Intent(this, KitchenBook::class.java)
            startActivity(intent)
        }

        val buttonClick4 = findViewById<ImageButton>(R.id.button4)
        buttonClick4.setOnClickListener {
            val intent = Intent(this, Progress::class.java)
            startActivity(intent)
        }

        val buttonClick5 = findViewById<ImageButton>(R.id.button5)
        buttonClick5.setOnClickListener {
            val intent = Intent(this, Goals::class.java)
            startActivity(intent)
        }
    }
}