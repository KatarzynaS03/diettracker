package edu.ib.diettracker.goals

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import edu.ib.diettracker.MainActivity
import edu.ib.diettracker.R
import edu.ib.diettracker.dietdairy.DietDairy

class Goals : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.goal)

        val height = findViewById<EditText>(R.id.wzrost)
        val curMas = findViewById<EditText>(R.id.obecnaWaga)
        val dremMas = findViewById<EditText>(R.id.porzadanaWaga)

        val date = findViewById<EditText>(R.id.koniecDiety)
        val save = findViewById<Button>(R.id.zapisz)

        save.setOnClickListener {
            //tu sie cos pierdoli nie wiem co z ta logika tego gownanego kodu
            if (height?.text.toString().toDouble() == null){
                Toast.makeText(this, "Enter your height", Toast.LENGTH_LONG).show()
            }
            else if(curMas?.text.toString().toDouble() == null){
                Toast.makeText(this, "Enter your current weight", Toast.LENGTH_LONG).show()
            }
            else if(dremMas?.text.toString().toDouble() == null){
                Toast.makeText(this, "Enter your desired weight", Toast.LENGTH_LONG).show()
            }
            else if(date?.text.toString() == null){
                Toast.makeText(this, "Enter date", Toast.LENGTH_LONG).show()
            }
            else{
                Toast.makeText(this, "Data was saved successfull", Toast.LENGTH_LONG).show()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }
    }
}